public class MobileNumbers {
    private String mobileNo;
    private String internationalNo;
    private String operator;

    public MobileNumbers(String mobile, String international, String operatorName) {
        this.mobileNo = mobile;
        this.internationalNo = international;
        this.operator = operatorName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getInternationalNo() {
        return internationalNo;
    }

    public String getOperator() {
        return operator;
    }
}
